#!/bin/bash

## gitlab-put-certs.sh
## Requires jq (https://stedolan.github.io/jq/)
## Utilize GitLab API to update pages certificates.
## Can be run as a CertBot manual-cleanup-hook or manually.

## firt domain listed in your ini
domain="[domain]"

## certDir is the location where CertBot wrote the "live" certificates
certDir="[someplace/live/]"

## ProjectID and API token for the GitLab  
glProjectId="[pages Project ID]"
glToken="[GitLab API Token]"

## Pages/Domain API URI for the project.  Shouldn't need edit.
glAPI="https://gitlab.com/api/v4/projects/${glProjectId}/pages/domains"

## Find the correct live location.
certDir=`ls -1tr ${liveDir} | grep ${domain} | tail -1`
certDir="${liveDir}${certDir}"

## Cycle through domains for a GitLab project and update their certificates.
for i in `curl -s --header "PRIVATE-TOKEN: ${glToken}" ${glAPI} | jq ".[].domain" | sed 's/"//g'`; do
  echo "Updating certificate for: $i"
  curl -s --request PUT --header "PRIVATE-TOKEN: ${glToken}" --form "certificate=@${certDir}/fullchain.pem" --form "key=@${certDir}/privkey.pem" ${glAPI}/$i | jq .
done
